<?php

namespace Drupal\dropbutton_as_modal\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Class DropbuttonAsModal.
 *
 * @package Drupal\dropbutton_as_modal\Form
 */
class DropbuttonAsModal extends ConfigFormBase {
  protected $database;

  private $array_exclude = ['field_comments'];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('database'));
  }

  /**
   * Construct a form.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dropbutton_as_modal.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dropbutton_as_modal_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['#tree'] = TRUE;
    $form['#attributes']['enctype'] = "multipart/form-data";
    $config = $this->config('dropbutton_as_modal.settings');
    $values = $config->get('settings');

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    $options = [];
    if (!empty($types)) {
      foreach ($types as $key => $value) {
        $options[$key] = $value->get('name');
      }
    }

    $fields = [];
    $fields['Nodes'] = [];
    $fields['Paragraphs'] = [];

    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    if (!empty($types)) {
      foreach ($types as $type_name => $type) {
        $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $type_name);

        foreach (array_keys($definitions) as $field_name) {
          if (substr($field_name, 0, 6) == 'field_' && !in_array($field_name, $this->array_exclude)) {
            $cardinality = $definitions[$field_name]->getFieldStorageDefinition()->getCardinality();
            if ($cardinality == -1) {
              $fields['Nodes'][$field_name] = $definitions[$field_name]->getLabel();
            }
          }
        }
      }
    }

    $entityManager = \Drupal::service('entity_type.bundle.info');
    $bundles = $entityManager->getBundleInfo('paragraph');
    foreach ($bundles as $key => $value) {
      $entity = \Drupal::service('entity_field.manager')->getFieldDefinitions('paragraph', $key);
      $fields['Paragraphs'][$value['label']] = [];

      foreach ($entity as $field) {
        $field_name = $field->getName();
        if (substr($field_name, 0, 6) == 'field_') {
          $cardinality = $field->getFieldStorageDefinition()->getCardinality();
          if ($cardinality == -1) {
            $fields['Paragraphs'][$value['label']][$field_name] = $field->getLabel();
          }
        }
      }
      if (empty($fields['Paragraphs'][$value['label']])) {
        unset($fields['Paragraphs'][$value['label']]);
      }
    }

    $styles = ImageStyle::loadMultiple();
    $options = [];
    if (!empty($styles)) {
      foreach ($styles as $machine_name => $style) {
        $options[$machine_name] = $style->get('label');
      }
    }

    $form['thumb_image_default'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Image Default'),
      '#upload_location' => 'public://dropbutton_as_modal',
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [256000000],
      ],
      '#theme' => 'tbs_thumb_upload',
      '#default_value' => $values['thumb_image_default'] ?? '',
    ];

    $form['thumb_image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Style'),
      '#options' => $options,
      '#default_value' => $values['thumb_image_style'] ?? '',
    ];

    $form['items_per_row'] = [
      '#type' => 'select',
      '#title' => $this->t('Items pe Row'),
      '#options' => [
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
      ],
      '#default_value' => $values['items_per_row'] ?? 4,
    ];

    $num_paragraph_field = $form_state->get('num_paragraph_field');
    if ($num_paragraph_field === NULL) {
      $num_paragraph_field = isset($values['paragraph_field']) ? count($values['paragraph_field']) : 0;
      $form_state->set('num_paragraph_field', $num_paragraph_field);
    }

    $num_paragraph_field = $form_state->get('num_paragraph_field');
    $form['paragraph_field'] = [
      '#type' => 'details',
      '#title' => $this->t('Config Paragraph'),
      '#prefix' => '<div id="wrapper-paragraph">',
      '#suffix' => '</div>',
      '#open' => !empty($fields['Paragraphs']),
      '#tree' => TRUE,

    ];

    $form['paragraph_field']['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Paragraph Field Name'),
        $this->t('Paragraph Button Label'),
        $this->t('Paragraph Dropbutton Hide'),
        $this
          ->t('Weight'),
      ],
      '#empty' => $this
        ->t('Sorry, There are no items!'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#tree' => TRUE,
    ];

    $form['paragraph_field']['table']['#attributes']['class'][] = 'table-dropbutton';
    if (!empty($fields['Paragraphs'])) {
      for ($i = 0; $i < $num_paragraph_field; $i++) {
        $form['paragraph_field']['table'][$i]['#attributes']['class'][] = 'draggable';
        $form['paragraph_field']['table'][$i]['#weight'] = $i ?? 0;

        $form['paragraph_field']['table'][$i]['class'] = [
          '#type' => 'select',
          '#title' => $this->t('Field Name'),
          '#options' => $fields['Paragraphs'],
          '#default_value' => $values['paragraph_field'][$i]['class'] ?? '',
        ];

        $form['paragraph_field']['table'][$i]['label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Button Label'),
          '#default_value' => $values['paragraph_field'][$i]['label'] ?? 'Add Component',
        ];

        $form['paragraph_field']['table'][$i]['hide'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Hide Default Dropbutton'),
          '#default_value' => $values['paragraph_field'][$i]['hide'] ? TRUE : FALSE,
        ];

        $form['paragraph_field']['table'][$i]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => isset($i) ? $i : '']),
          '#title_display' => 'invisible',
          '#default_value' => $i ?? 0,
          '#attributes' => [
            'class' => [
              'table-sort-weight',
            ],
          ],
        ];
      }
    }
    $form['paragraph_field']['actions'] = [
      '#type' => 'actions',
    ];

    $form['paragraph_field']['actions']['add_paragraph'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Paragraph Field'),
      '#submit' => ['::addOneParagraphField'],
      '#ajax' => [
        'callback' => '::addmoreCallbackParagraphField',
        'wrapper' => 'wrapper-paragraph',
      ],
    ];

    if ($num_paragraph_field > 0) {
      $form['paragraph_field']['actions']['remove_paragraph'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove Paragraph Field'),
        '#submit' => ['::removeCallbackParagraphField'],
        '#ajax' => [
          'callback' => '::addmoreCallbackParagraphField',
          'wrapper' => 'wrapper-paragraph',
        ],
      ];
    }

    $num_node_field = $form_state->get('num_node_field');
    if ($num_node_field === NULL) {
      $num_node_field = isset($values['node_field']) ? count($values['node_field']) : 0;
      $form_state->set('num_node_field', $num_node_field);
    }

    $num_node_field = $form_state->get('num_node_field');
    $form['node_field'] = [
      '#type' => 'details',
      '#title' => $this->t('Config node'),
      '#prefix' => '<div id="wrapper-node">',
      '#suffix' => '</div>',
      '#open' => !empty($fields['Nodes']),
    ];

    $form['node_field']['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Node Field Name'),
        $this->t('Node Button Label'),
        $this->t('Node Dropbutton Hide'),
        $this
          ->t('Weight'),
      ],
      '#empty' => $this
        ->t('Sorry, There are no items!'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['node_field']['table']['#attributes']['class'][] = 'table-dropbutton';
    if (!empty($fields['Nodes'])) {
      for ($i = 0; $i < $num_node_field; $i++) {
        $form['node_field']['table'][$i]['#attributes']['class'][] = 'draggable';
        $form['node_field']['table'][$i]['#weight'] = $i ?? 0;

        $form['node_field']['table'][$i]['class'] = [
          '#type' => 'select',
          '#title' => $this->t('Name'),
          '#options' => $fields['Nodes'],
          '#default_value' => $values['node_field'][$i]['class'] ?? '',
        ];

        $form['node_field']['table'][$i]['label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Button Label'),
          '#default_value' => $values['node_field'][$i]['label'] ?? 'Add Component',
        ];

        $form['node_field']['table'][$i]['hide'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Hide Default Dropbutton'),
          '#default_value' => $values['node_field'][$i]['hide'] ? TRUE : FALSE,
        ];

        $form['node_field']['table'][$i]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => isset($i) ? $i : '']),
          '#title_display' => 'invisible',
          '#default_value' => $i ?? 0,
          '#attributes' => [
            'class' => [
              'table-sort-weight',
            ],
          ],
        ];
      }
    }
    $form['node_field']['actions'] = [
      '#type' => 'actions',
    ];

    $form['node_field']['actions']['add_node'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add node Field'),
      '#submit' => ['::addOneNodeField'],
      '#ajax' => [
        'callback' => '::addmoreCallbackNodeField',
        'wrapper' => 'wrapper-node',
      ],
    ];

    if ($num_node_field > 0) {
      $form['node_field']['actions']['remove_node'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove node Field'),
        '#submit' => ['::removeCallbackNodeField'],
        '#ajax' => [
          'callback' => '::addmoreCallbackNodeField',
          'wrapper' => 'wrapper-node',
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function addmoreCallbackNodeField(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_node_field');
    $add_button = $name_field + 1;
    $form_state->set('num_node_field', $add_button);
    return $form['node_field'];
  }

  /**
   * {@inheritdoc}
   */
  public function addOneNodeField(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_node_field');
    $add_button = $name_field + 1;
    $form_state->set('num_node_field', $add_button);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function removeCallbackNodeField(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_node_field');
    if ($name_field > 0) {
      $remove_button = $name_field - 1;
      $form_state->set('num_node_field', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function addmoreCallbackParagraphField(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_paragraph_field');
    $add_button = $name_field + 1;
    $form_state->set('num_paragraph_field', $add_button);
    return $form['paragraph_field'];
  }

  /**
   * {@inheritdoc}
   */
  public function addOneParagraphField(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_paragraph_field');
    $add_button = $name_field + 1;
    $form_state->set('num_paragraph_field', $add_button);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function removeCallbackParagraphField(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_paragraph_field');
    if ($name_field > 0) {
      $remove_button = $name_field - 1;
      $form_state->set('num_paragraph_field', $remove_button);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();
    $output = $this->t('Configs are been saved!');
    $this->messenger()->addMessage($output);

    $storage = [];

    $storage['paragraph_field'] = [];
    if (!empty($form_values['paragraph_field']['table'])) {
      usort($form_values['paragraph_field']['table'], [$this, 'sortByWeight']);
      foreach ($form_values['paragraph_field']['table'] as $value) {
        $storage['paragraph_field'][] = [
          'class' => $value['class'],
          'hide' => $value['hide'],
          'label' => $value['label'],
        ];
      }
    }

    $storage['node_field'] = [];
    if (!empty($form_values['node_field']['table'])) {
      usort($form_values['node_field']['table'], [$this, 'sortByWeight']);
      foreach ($form_values['node_field']['table'] as $value) {
        $storage['node_field'][] = [
          'class' => $value['class'],
          'hide' => $value['hide'],
          'label' => $value['label'],
        ];
      }
    }

    $storage['thumb_image_style'] = $form_values['thumb_image_style'];

    if (!empty($form_values['thumb_image_default'])) {
      $storage['thumb_image_default'] = $form_values['thumb_image_default'];
    }

    if (!empty($form_values['items_per_row'])) {
      $storage['items_per_row'] = $form_values['items_per_row'];
    }

    $this->config('dropbutton_as_modal.settings')
      ->set('settings', $storage)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  private  function sortByWeight($a, $b) {
    return ($a["weight"] <= $b["weight"]) ? -1 : 1;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

}
