(function ($, Drupal, drupalSettings) {
  var items_per_row = drupalSettings.dropbutton_as_modal.settings.items_per_row;
  var default_thumb = drupalSettings.dropbutton_as_modal.settings.thumb_default;
  var modal = `<div id="dropbutton-as-modal-dialog" class="dropbutton-as-modal-dialog">
      <div>
          <a href="#close" title="Close" class="dropbutton-as-modal-dialog-close">X</a>
          <input type="text" class="dropbutton-as-modal-dialog-search" placeholder="${Drupal.t('Search component')}">
          <div class="dropbutton-as-modal-dialog-results"></div>
      </div>
  </div>` ;

  var modalCol = `<div class="dropbutton-as-modal-dialog-col" data-col="${items_per_row}">
      <div style="background-image: url('${default_thumb}')" class="img"></div>
      <p></p>
  </div>`;

  var $component = $('.dropbutton-as-modal-dialog');
  var $modal = $(modal);
  if (!$component.length) {
    $('body').append($modal);
  } else {
    $modal = $('#dropbutton-as-modal-dialog');
  }

  $modal.on('keyup', function (e) {
    if (e.code == 'Escape') {
      $modal.removeClass('active');
    }
  });

  $modal.find('.dropbutton-as-modal-dialog-close').on('click', function (e) {
    $modal.removeClass('active');
  });

  $modal.find('.dropbutton-as-modal-dialog-search').on('keyup', function (e) {
    var val = this.value.trim();
    if (val) {
      $modal.find('.dropbutton-as-modal-dialog-col').each(function () {
        if (!$(this).find('p').text().trim().match(new RegExp(val, 'i'))) {
          $(this).hide();
        } else {
          $(this).show();
        }
      })
    } else {
      $modal.find('.dropbutton-as-modal-dialog-col').show();
    }
  });

  function modal_add_items($items, field_name) {
    $modal.find('.dropbutton-as-modal-dialog-results').html('');
    $items.each(function () {
      var $action = $(this);
      var paragraph_name = $action.attr('name').split(field_name + '_')[1].replace('_add_more', '');
      var thumb = drupalSettings.dropbutton_as_modal.settings.paragraphs[paragraph_name];

      var $modalCol = $(modalCol);
      $modalCol.find('p').text($(this).val().replace('Add', ''));
      $modalCol.attr('title', $(this).val());

      if (thumb !== undefined) {
        $modalCol.find('.img').css('background-image', "url('" + thumb + "')")
      }

      $modal.find('.dropbutton-as-modal-dialog-results').append($modalCol);
      $modalCol.on('click', function (e) {
        e.preventDefault();
        $action.trigger('mousedown');
        $modal.removeClass('active');
        $modal.find('.dropbutton-as-modal-dialog-results').html('');
      });
    });
  }

  Drupal.behaviors.dropbutton_as_modal_field__widget_entity_reference_paragraphs = {
    attach: function attach() {
      if (drupalSettings.dropbutton_as_modal.settings.fields.length) {
        for (field in drupalSettings.dropbutton_as_modal.settings.fields) {
          var field_name = drupalSettings.dropbutton_as_modal.settings.fields[field].class;
          var hide = drupalSettings.dropbutton_as_modal.settings.fields[field].hide;
          var label = drupalSettings.dropbutton_as_modal.settings.fields[field].label;
          $(
            '.field--widget-entity-reference-paragraphs [data-drupal-selector="edit-' + field_name.replace(/\_/g, '-') + '"] > div.clearfix > .dropbutton-wrapper,' +
            '.field--widget-entity-reference-paragraphs [data-drupal-selector="edit-' + field_name.replace(/\_/g, '-') + '"] > .dropbutton-wrapper,' +

            '.field--widget-entity-reference-paragraphs [data-drupal-selector$="subform-' + field_name.replace(/\_/g, '-') + '"] > div.clearfix > .dropbutton-wrapper,' +
            '.field--widget-entity-reference-paragraphs [data-drupal-selector$="subform-' + field_name.replace(/\_/g, '-') + '"] > .dropbutton-wrapper'

          ).each(function () {
            var $this = $(this);
            var name = field_name;
            if (hide) {
              $this.hide();
            }
            var $add_component = $this.closest('.form-wrapper').find('.wrapper-button-dropbutton-as-modal-dialog button[data-' + name + ']');
            if (!$add_component.length) {
              var $add_component = $('<span class="wrapper-button-dropbutton-as-modal-dialog" style="margin-right: 10px;"> <button data-' + name + ' typ="button" class="button--small button" style="margin-right: 10px; padding: 4px 1.5em;">' + Drupal.t(label) + '</button> </span>');
              $add_component.insertBefore($this);
              $add_component.find('button').once().on('click', function (e) {
                e.preventDefault();
                modal_add_items($this.find('.dropbutton__item input'), name);

                $modal.addClass('active');
                setTimeout(function () {
                  $modal.find('input')[0].focus();
                }, 200)
              });
            }
          });

          $(
            '.field--widget-paragraphs [data-drupal-selector="edit-' + field_name.replace(/\_/g, '-') + '"] > div.clearfix .paragraph-type-add-modal,' +
            '.field--widget-paragraphs [data-drupal-selector="edit-' + field_name.replace(/\_/g, '-') + '"] > div.clearfix .paragraphs-dropbutton-wrapper, ' +

            '.field--widget-paragraphs [data-drupal-selector$="subform-' + field_name.replace(/\_/g, '-') + '"] > div.clearfix .paragraph-type-add-modal,' +
            '.field--widget-paragraphs [data-drupal-selector$="subform-' + field_name.replace(/\_/g, '-') + '"] > div.clearfix .paragraphs-dropbutton-wrapper'
          ).each(function () {
            var $this = $(this);
            var name = field_name;
            if (hide) {
              $this.hide();
            }
            var $add_component = $this.closest('.form-item').find('.wrapper-button-dropbutton-as-modal-dialog button[data-' + name + ']');
            if (!$add_component.length) {
              var $add_component = $('<span class="wrapper-button-dropbutton-as-modal-dialog" style="margin-right: 10px;"> <button data-' + name + ' typ="button" class="button--small button" style="margin-right: 10px; padding: 4px 1.5em;">' + Drupal.t(label) + '</button> </span>');
              $add_component.insertBefore($this);
              $add_component.find('button').on('click', function (e) {
                e.preventDefault();
                modal_add_items($this.find('.paragraphs-add-dialog-row input, .secondary-action input',), name)
                $modal.addClass('active');
                setTimeout(function () {
                  $modal.find('input')[0].focus();
                }, 200)
              });
            }
          });

        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
